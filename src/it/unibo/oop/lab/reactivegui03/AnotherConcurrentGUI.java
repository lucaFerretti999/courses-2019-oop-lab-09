package it.unibo.oop.lab.reactivegui03;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.*;
/**
 * 
 * @author luca.ferretti8
 *
 */
public class AnotherConcurrentGUI extends JFrame{
	private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    
    public AnotherConcurrentGUI() {
    	super();
    	final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        final Agent agent = new Agent();
        final AgentStops agentStops = new AgentStops(agent);
        new Thread(agent).start();
        new Thread(agentStops).start();
        stop.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				agent.stopCounting();
				stopAll();
				
			}
		});
        down.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				agent.downCounting();

			}
		});
        up.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				agent.upCounting();
			}
		});
    }

       private class Agent implements Runnable {
        	private volatile boolean goes_up = true;
        	private volatile int cont=0;
        	private volatile boolean stop = false;

			public void run() {
				 while (!this.stop) {
		                try {
		                    SwingUtilities.invokeAndWait(new Runnable() {
		                        @Override
		                        public void run() {
		                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.cont));
		                        }
		                    });
		                    if (goes_up) {
		                    this.cont++;
		                    }
		                    else {
		                    	this.cont--;
		                    }
		                    Thread.sleep(100);
		                } catch (InvocationTargetException | InterruptedException ex) {
		                    ex.printStackTrace();
		                }
		            } 
			}

			public void upCounting() {
				this.goes_up=true;
			}
			public void downCounting() {
				this.goes_up = false;
			}
			public void stopCounting() {
				this.stop=true;
			}
        }
       
       private class AgentStops implements Runnable{

    	   private Agent agent;
    	   public AgentStops(final Agent agent) {
    		   this.agent=agent;
    	   }
    	   
		public void run() {
			try {
				Thread.sleep(10000);
				AnotherConcurrentGUI.this.stopAll();
				agent.stopCounting();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
       }
	
   	public void stopAll() {
		this.stop.setEnabled(false);
		this.down.setEnabled(false);
		this.up.setEnabled(false);
		
	}
	
	

}
